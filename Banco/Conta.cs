﻿using System;

namespace Banco
{
    public class Conta
    {
        public int Numero { get; set; }
        public double Saldo { get; private set; }
        internal Cliente Titular { get; set; }

        public Conta()
        {

        }

        public Conta(int numero)
        {
            this.Numero = numero;
        }

        public Conta(int numero, double saldo, Cliente titular)
        {
            this.Numero = numero;
            this.Saldo = saldo;
            this.Titular = titular;
        }

        public virtual void Deposita(double valor)
        {
            this.Saldo += valor;
        }

        public virtual void Saca(double valor)
        {
            this.Saldo -= valor;
        }

        public virtual void Transferência(Conta contaDestino, double valor)
        {
            this.Saca(valor);
            contaDestino.Deposita(valor);
        }
    }
}