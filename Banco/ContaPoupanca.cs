﻿namespace Banco
{
    internal class ContaPoupanca : Conta
    {


        public ContaPoupanca(int numero) : base(numero) { }

        public ContaPoupanca(int numero, double saldo, Cliente titular) : base(numero, saldo, titular) { }
         
        public override void Saca(double valor)
        {
            base.Saca(valor + 0.10);
        }

    }
}
