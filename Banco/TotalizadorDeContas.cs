﻿namespace Banco
{
    class TotalizadorDeContas
    {
        public double SaldoTotal { get; set; }

        public void Adiciona(Conta conta)
        {
            SaldoTotal += conta.Saldo;
        }

        public double MostraSaldo()
        {
            return this.SaldoTotal;
        }

    }
}
