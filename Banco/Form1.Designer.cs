﻿namespace Banco
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtTitular = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.txtSaldo = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblSaldo = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.btnDepositar = new System.Windows.Forms.Button();
            this.btnSacar = new System.Windows.Forms.Button();
            this.grpConta = new System.Windows.Forms.GroupBox();
            this.btnTotalSaldo = new System.Windows.Forms.Button();
            this.lblIndice = new System.Windows.Forms.Label();
            this.grpBuscaDeConta = new System.Windows.Forms.GroupBox();
            this.btnTransferencia = new System.Windows.Forms.Button();
            this.txtContaDestino = new System.Windows.Forms.Label();
            this.comboContaDestinoTransferencia = new System.Windows.Forms.ComboBox();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.BtnNovaConta = new System.Windows.Forms.Button();
            this.grpConta.SuspendLayout();
            this.grpBuscaDeConta.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTitular
            // 
            this.txtTitular.Location = new System.Drawing.Point(154, 39);
            this.txtTitular.Name = "txtTitular";
            this.txtTitular.Size = new System.Drawing.Size(171, 26);
            this.txtTitular.TabIndex = 0;
            this.txtTitular.TextChanged += new System.EventHandler(this.TxtTitular_TextChanged);
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(154, 71);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(171, 26);
            this.txtNumero.TabIndex = 1;
            // 
            // txtSaldo
            // 
            this.txtSaldo.Location = new System.Drawing.Point(154, 103);
            this.txtSaldo.Name = "txtSaldo";
            this.txtSaldo.Size = new System.Drawing.Size(171, 26);
            this.txtSaldo.TabIndex = 2;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(25, 39);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(52, 20);
            this.lblNome.TabIndex = 3;
            this.lblNome.Text = "Titular";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Location = new System.Drawing.Point(25, 71);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(65, 20);
            this.lblNumero.TabIndex = 4;
            this.lblNumero.Text = "Número";
            this.lblNumero.Click += new System.EventHandler(this.Label2_Click);
            // 
            // lblSaldo
            // 
            this.lblSaldo.AutoSize = true;
            this.lblSaldo.Location = new System.Drawing.Point(27, 103);
            this.lblSaldo.Name = "lblSaldo";
            this.lblSaldo.Size = new System.Drawing.Size(50, 20);
            this.lblSaldo.TabIndex = 5;
            this.lblSaldo.Text = "Saldo";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(154, 136);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(171, 26);
            this.txtValor.TabIndex = 6;
            this.txtValor.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Location = new System.Drawing.Point(27, 136);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(46, 20);
            this.lblValor.TabIndex = 7;
            this.lblValor.Text = "Valor";
            // 
            // btnDepositar
            // 
            this.btnDepositar.Location = new System.Drawing.Point(29, 214);
            this.btnDepositar.Name = "btnDepositar";
            this.btnDepositar.Size = new System.Drawing.Size(87, 30);
            this.btnDepositar.TabIndex = 8;
            this.btnDepositar.Text = "Depositar";
            this.btnDepositar.UseVisualStyleBackColor = true;
            this.btnDepositar.Click += new System.EventHandler(this.BtnDepositar2_Click);
            // 
            // btnSacar
            // 
            this.btnSacar.Location = new System.Drawing.Point(123, 214);
            this.btnSacar.Name = "btnSacar";
            this.btnSacar.Size = new System.Drawing.Size(75, 30);
            this.btnSacar.TabIndex = 9;
            this.btnSacar.Text = "Sacar";
            this.btnSacar.UseVisualStyleBackColor = true;
            this.btnSacar.Click += new System.EventHandler(this.BtnSacar_Click);
            // 
            // grpConta
            // 
            this.grpConta.Controls.Add(this.BtnNovaConta);
            this.grpConta.Controls.Add(this.btnTotalSaldo);
            this.grpConta.Controls.Add(this.txtSaldo);
            this.grpConta.Controls.Add(this.btnSacar);
            this.grpConta.Controls.Add(this.txtTitular);
            this.grpConta.Controls.Add(this.btnDepositar);
            this.grpConta.Controls.Add(this.txtNumero);
            this.grpConta.Controls.Add(this.lblValor);
            this.grpConta.Controls.Add(this.lblNome);
            this.grpConta.Controls.Add(this.txtValor);
            this.grpConta.Controls.Add(this.lblNumero);
            this.grpConta.Controls.Add(this.lblSaldo);
            this.grpConta.Location = new System.Drawing.Point(47, 246);
            this.grpConta.Name = "grpConta";
            this.grpConta.Size = new System.Drawing.Size(440, 260);
            this.grpConta.TabIndex = 10;
            this.grpConta.TabStop = false;
            this.grpConta.Text = "Conta";
            this.grpConta.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // btnTotalSaldo
            // 
            this.btnTotalSaldo.Location = new System.Drawing.Point(205, 214);
            this.btnTotalSaldo.Name = "btnTotalSaldo";
            this.btnTotalSaldo.Size = new System.Drawing.Size(104, 30);
            this.btnTotalSaldo.TabIndex = 10;
            this.btnTotalSaldo.Text = "TotalSaldo";
            this.btnTotalSaldo.UseVisualStyleBackColor = true;
            this.btnTotalSaldo.Click += new System.EventHandler(this.BtnTotalSaldo_Click);
            // 
            // lblIndice
            // 
            this.lblIndice.AutoSize = true;
            this.lblIndice.Location = new System.Drawing.Point(25, 28);
            this.lblIndice.Name = "lblIndice";
            this.lblIndice.Size = new System.Drawing.Size(123, 20);
            this.lblIndice.TabIndex = 12;
            this.lblIndice.Text = "Escolha a conta";
            // 
            // grpBuscaDeConta
            // 
            this.grpBuscaDeConta.Controls.Add(this.btnTransferencia);
            this.grpBuscaDeConta.Controls.Add(this.txtContaDestino);
            this.grpBuscaDeConta.Controls.Add(this.comboContaDestinoTransferencia);
            this.grpBuscaDeConta.Controls.Add(this.comboBox);
            this.grpBuscaDeConta.Controls.Add(this.lblIndice);
            this.grpBuscaDeConta.Location = new System.Drawing.Point(47, 12);
            this.grpBuscaDeConta.Name = "grpBuscaDeConta";
            this.grpBuscaDeConta.Size = new System.Drawing.Size(344, 175);
            this.grpBuscaDeConta.TabIndex = 11;
            this.grpBuscaDeConta.TabStop = false;
            this.grpBuscaDeConta.Text = "Busca de Contas";
            // 
            // btnTransferencia
            // 
            this.btnTransferencia.Location = new System.Drawing.Point(29, 128);
            this.btnTransferencia.Name = "btnTransferencia";
            this.btnTransferencia.Size = new System.Drawing.Size(119, 33);
            this.btnTransferencia.TabIndex = 16;
            this.btnTransferencia.Text = "Transferência";
            this.btnTransferencia.UseVisualStyleBackColor = true;
            this.btnTransferencia.Click += new System.EventHandler(this.BtnTransferencia_Click);
            // 
            // txtContaDestino
            // 
            this.txtContaDestino.AutoSize = true;
            this.txtContaDestino.Location = new System.Drawing.Point(29, 82);
            this.txtContaDestino.Name = "txtContaDestino";
            this.txtContaDestino.Size = new System.Drawing.Size(111, 20);
            this.txtContaDestino.TabIndex = 15;
            this.txtContaDestino.Text = "Conta Destino";
            // 
            // comboContaDestinoTransferencia
            // 
            this.comboContaDestinoTransferencia.FormattingEnabled = true;
            this.comboContaDestinoTransferencia.Location = new System.Drawing.Point(155, 82);
            this.comboContaDestinoTransferencia.Name = "comboContaDestinoTransferencia";
            this.comboContaDestinoTransferencia.Size = new System.Drawing.Size(170, 28);
            this.comboContaDestinoTransferencia.TabIndex = 14;
            this.comboContaDestinoTransferencia.SelectedIndexChanged += new System.EventHandler(this.ComboContaDestinoTransferencia_SelectedIndexChanged);
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(154, 28);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(171, 28);
            this.comboBox.TabIndex = 13;
            this.comboBox.SelectedIndexChanged += new System.EventHandler(this.ComboContas);
            // 
            // BtnNovaConta
            // 
<<<<<<< HEAD
            this.BtnNovaConta.Location = new System.Drawing.Point(315, 195);
            this.BtnNovaConta.Name = "BtnNovaConta";
            this.BtnNovaConta.Size = new System.Drawing.Size(92, 59);
            this.BtnNovaConta.TabIndex = 11;
            this.BtnNovaConta.Text = "Cadastrar Conta";
=======
            this.BtnNovaConta.Location = new System.Drawing.Point(47, 513);
            this.BtnNovaConta.Name = "BtnNovaConta";
            this.BtnNovaConta.Size = new System.Drawing.Size(126, 32);
            this.BtnNovaConta.TabIndex = 12;
            this.BtnNovaConta.Text = "Nova Conta";
>>>>>>> c6eeac40ddf26aa1989023dbbc3cc843e1a500d8
            this.BtnNovaConta.UseVisualStyleBackColor = true;
            this.BtnNovaConta.Click += new System.EventHandler(this.BtnNovaConta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 598);
            this.Controls.Add(this.BtnNovaConta);
            this.Controls.Add(this.grpBuscaDeConta);
            this.Controls.Add(this.grpConta);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpConta.ResumeLayout(false);
            this.grpConta.PerformLayout();
            this.grpBuscaDeConta.ResumeLayout(false);
            this.grpBuscaDeConta.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtTitular;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.TextBox txtSaldo;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblSaldo;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Button btnDepositar;
        private System.Windows.Forms.Button btnSacar;
        private System.Windows.Forms.GroupBox grpConta;
        private System.Windows.Forms.Button btnTotalSaldo;
        private System.Windows.Forms.Label lblIndice;
        private System.Windows.Forms.GroupBox grpBuscaDeConta;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.Button btnTransferencia;
        private System.Windows.Forms.Label txtContaDestino;
        private System.Windows.Forms.ComboBox comboContaDestinoTransferencia;
        private System.Windows.Forms.Button BtnNovaConta;
    }
}

