﻿namespace Banco
{
    public class Cliente
    {
        public string Nome { get; set; }

        public Cliente()
        {
        }

        public Cliente(string nome)
        {
            this.Nome = nome;
        }
    }
}