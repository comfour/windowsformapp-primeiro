﻿namespace Banco
{
    class ContaCorrente : Conta
    {
        public ContaCorrente() : base() { }

        public ContaCorrente(int numero, double saldo, Cliente titular) : base(numero, saldo, titular) { }

       
        public override void Saca(double valor)
        {
            base.Saca(valor + 0.05);
        }

        public override void Deposita(double valor)
        {
            base.Deposita(valor - 0.10);
        }

    }
}
